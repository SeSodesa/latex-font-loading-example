# LaTeX font loading example

This repo contains an example on how to load fonts from a local subfolder
`fonts/` with LuaLaTeΧ and `fontspec`. The package `fourier` is also loaded to
make math pretty. Compile with

	lualatex main.tex && lualatex main.tex

since `pdflatex` does not work with `fontspec`.
